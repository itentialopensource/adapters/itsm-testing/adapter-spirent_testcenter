# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Spirent_testcenter System. The API that was used to build the adapter for Spirent_testcenter is usually available in the report directory of this adapter. The adapter utilizes the Spirent_testcenter API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Spirent TestCenter adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Spirent's TestCenter.

With this adapter you have the ability to perform operations with Spirent TestCenter on items such as:

- Objects
- Files
- Chassis

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
