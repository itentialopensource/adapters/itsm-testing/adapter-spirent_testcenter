# Spirent_testcenter

Vendor: Spirent
Homepage: https://www.spirent.com/

Product: TestCenter
Product Page: https://www.spirent.com/products#test

## Introduction
We classify Spirent TestCenter into the ITSM domain as Spirent TestCenter is a high-performance network testing solution that provides comprehensive testing and validation of network infrastructure, devices, and services.

## Why Integrate
The Spirent TestCenter adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Spirent's TestCenter.

With this adapter you have the ability to perform operations with Spirent TestCenter on items such as:

- Objects
- Files
- Chassis

## Additional Product Documentation
The [API documents for Spirent TestCenter](https://support.spirent.com/SC_KnowledgeView?id=DOC10035)